#pragma once
#include "IDataAccess.h" 
#include "sqlite3.h"
#include <io.h>
#include <vector>
#include "MyException.h"

static std::vector<std::string> dataHolder;
int callBack(void *data, int argc, char** argv, char** azColName);

class DataBaseAccess : public IDataAccess
{
	// album related
	virtual const std::list<Album> getAlbums(); //work
	virtual const std::list<Album> getAlbumsOfUser(const User& user);//work
	virtual void createAlbum(const Album& album);//work
	virtual void deleteAlbum(const std::string& albumName, int userId); //work
	virtual bool doesAlbumExists(const std::string& albumName, int userId);//work
	virtual Album openAlbum(const std::string& albumName); //work
	virtual void closeAlbum(Album& pAlbum); //work
	virtual void printAlbums();//work

	// picture related
	virtual void addPictureToAlbumByName(const std::string& albumName, const Picture& picture); //work
	virtual void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName); //work
	virtual void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId); //work
	virtual void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId); //work

	// user related
	virtual void printUsers(); //work
	virtual User getUser(int userId); //work
	virtual void createUser(User& user);//work
	virtual void deleteUser(const User& user);//work
	virtual bool doesUserExists(int userId);//work


	// user statistics
	virtual int countAlbumsOwnedOfUser(const User& user); // work
	virtual int countAlbumsTaggedOfUser(const User& user); //work
	virtual int countTagsOfUser(const User& user); //work
	virtual float averageTagsPerAlbumOfUser(const User& user); // work

	// queries
	virtual User getTopTaggedUser(); // to do
	virtual Picture getTopTaggedPicture(); // to do
	virtual std::list<Picture> getTaggedPicturesOfUser(const User& user); // to do

	virtual bool open();//work
	virtual void close();//work
	virtual void clear();//work

	virtual int getPictureId(); //work
	virtual int getUserId(); //work

private: 
	sqlite3* db;
	Album* opened_AL;
	void selectBy(std::string src, std::string byWhat, std::string what, sqlite3* db);//work
	void deleteBy(std::string src, std::string byWhat, sqlite3* db); //work
	void insertTo(std::string toWhere, std::string headers, std::string what, sqlite3* db);//work
	void updateAl(Album* Al);//work
};