#define _CRT_SECURE_NO_WARNINGS
#include "DataBaseAccess.h"

/*
the function open the data base. in case it dosent exist, create new one.
input: NONE
output : T | F if successeded to open
*/
bool DataBaseAccess::open()
{
	//try to open the file
	std::string dbFileName = "GalleryDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &this->db);
	char* sqlite3_errmsg;
	//if the proc failed, print relevent msg.
	if (res != SQLITE_OK)
	{
		db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}
	//if it dosent exist, create one
	if (doesFileExist != 0) {

		std::string sql = "CREATE TABLE ALBUMS(ID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, NAME TEXT NOT NULL UNIQUE, USER_ID INTEGER NOT NULL, CREATION_DATE TEXT NOT NULL, FOREIGN KEY(USER_ID) REFERENCES USERS(ID));"\
		"CREATE TABLE PICTURES(ID  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, NAME TEXT  NOT NULL, LOCATION TEXT NOT NULL, CREATION_DATE TEXT NOT NULL, ALBUM_ID INTEGER NOT NULL, FOREIGN KEY(ALBUM_ID) REFERENCES ALBUMS(ID));"\
		"CREATE TABLE TAGS(PICTURE_ID INTEGER NOT NULL, USER_ID INTEGER NOT NULL, PRIMARY KEY(PICTURE_ID, USER_ID), FOREIGN KEY(PICTURE_ID) REFERENCES PICTURES(ID), FOREIGN KEY(USER_ID) REFERENCES USERS(ID));"\
		"CREATE TABLE USERS(ID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, NAME TEXT NOT NULL);";
		res = sqlite3_exec(db, sql.c_str(), nullptr, nullptr, &sqlite3_errmsg);
	}
	dataHolder.clear();
	return true;
}

/*
the function close the database
input: NONE
output: NONE
*/
void DataBaseAccess::close() 
{
	//clear and close all the vars.
	dataHolder.clear();
	sqlite3_close(this->db);
	this->db = nullptr;
}

/*
the function delete the database
input: NONE
output: NONE
*/
void DataBaseAccess::clear()
{
	//create the sql msg
	char* sqlite3_errmsg;
	dataHolder.clear();
	std::string sql = "DELETE FROM TAGS;"\
		"DELETE FROM PICTURES;"\
		"DELETE FROM ALBUMS;"\
		"DELETE FROM USERS;";
	//send it
	int res = sqlite3_exec(this->db, sql.c_str(), nullptr, nullptr, &sqlite3_errmsg);
}

/*
the function get the last picture's id
input: NONE
output: the id of the last picture
*/
int DataBaseAccess::getPictureId()
{
	//create the sql msg which get all the id's of all the pictures in the data base
	char* sqlite3_errmsg = nullptr;
	std::string SQL = "SELECT ID FROM PICTURES";
	int res = sqlite3_exec(this->db, SQL.c_str(), callBack, nullptr, &sqlite3_errmsg), returnval = 100;//initial it as 100 in case the db is empty
	if (sqlite3_errmsg != nullptr)
	{
		std::cout << std::string(sqlite3_errmsg) << std::endl;
	}
	//if there is any picture
	if (dataHolder.size() != 0)
	{
		//get the last picture id and clean the data holder
		returnval = stoi(dataHolder[dataHolder.size() - 1]);
		dataHolder.clear();
	}
	//return the id
	return returnval;
}
/*
the function get the last user's id
input : NONE
output: NONE
*/
int DataBaseAccess::getUserId()
{
	//create the sql msg which get all the id's of all the users in the data base
	char* sqlite3_errmsg = nullptr;
	std::string SQL = "SELECT ID FROM USERS";
	int res = sqlite3_exec(this->db, SQL.c_str(), callBack, nullptr, &sqlite3_errmsg), returnval = 200;//initial it as 200 in case the db is empty
	if (sqlite3_errmsg != nullptr)
	{
		std::cout << std::string(sqlite3_errmsg) << std::endl;
	}
	//if there is any user
	if (dataHolder.size() != 0)
	{
		//get the last user id and clean the data holder
		returnval = stoi(dataHolder[dataHolder.size() - 1]);
		dataHolder.clear();
	}
	//get the last id
	return returnval;
}

/*
the function delete the pointer to the opened album
input: reference to the album
output: NONE
*/
void DataBaseAccess::closeAlbum(Album& album){
	delete(this->opened_AL);
}

/*
the function get all the albums and print them
input: NONE
output: NONE
*/
void DataBaseAccess::printAlbums()
{
	//get all the albums and create a list of them
	std::list<Album> albums = this->getAlbums();
	if (albums.empty()) {
		throw MyException("There are no existing albums.");
	}
	//print them one by one.
	std::cout << "Album list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (const Album& album : albums) {
		std::cout << "Creation time: " << album.getCreationDate() <<  " * " << album;
	}
}

/*
the function get all the albums from the data base, create a list of them and return it
input: NONE
output: list of albums
*/
const std::list<Album> DataBaseAccess::getAlbums()
{
	//create the list and the SQL msg
	char* sqlite3_errmsg;
	std::list<Album> * albums = new std::list<Album>();
	std::string SQL = "SELECT * FROM ALBUMS";
	int res = sqlite3_exec(this->db, SQL.c_str(), callBack, nullptr, &sqlite3_errmsg);
	//goes over the data, create albums and push them to the list
	for (size_t i = 0; i < dataHolder.size(); i += 4)
	{
		albums->push_back(Album(stoi(dataHolder[i + 2]), dataHolder[i + 1], dataHolder[i + 3]));
	}
	//clear the data holder
	dataHolder.clear();
	return *albums;
}
/*
	the function get all the albums of a specific user
	input: reference to the user
	output: list of his albums
*/
const std::list<Album> DataBaseAccess::getAlbumsOfUser(const User& user)
{
	//get all the albums
	std::list<Album> albumsOfUser, albums = this->getAlbums();
	//goes over the album list and push back every album with the same owner id as the user
	for (const auto& album : albums) {
		if (album.getOwnerId() == user.getId()) {
			albumsOfUser.push_back(album);
		}
	}
	//return the created list
	return albumsOfUser;
}
/*
the function gets an album and insert it to the data base.
input: the album
output: none
*/
void DataBaseAccess::createAlbum(const Album& album)
{
	//insert to the data base
	insertTo("ALBUMS", "(NAME, CREATION_DATE, USER_ID)", "(\"" + album.getName() + "\", \"" + album.getCreationDate() + "\", " + std::to_string(album.getOwnerId()) + ")", this->db);
	dataHolder.clear();
}

/*
the function delete an album and all what is related to it from the data base.
input: reference to the album name, the user id
output: NONE
*/
void DataBaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	//get album id
	selectBy("ALBUMS", "NAME = \"" + albumName + "\" AND USER_ID = " + std::to_string(userId), "ID", this->db);
	
	
	selectBy("PICTURES", "ALBUM_ID = " + dataHolder[0], "ID", this->db);
	//if there is any pictures
	if (dataHolder.size() > 1)
	{
		//delete tags
		for (size_t i = 1; i < dataHolder.size(); i++)
		{
			deleteBy("TAGS", "USER_ID = " + std::to_string(userId) + " AND PICTURE_ID = " + dataHolder[i], this->db);
		}
		//delete all the pictures from the album
		deleteBy("PICTURES", "ALBUM_ID = " + dataHolder[0], this->db);
	}

	//delete albums
	deleteBy("ALBUMS", "NAME = \"" + albumName + "\" AND USER_ID = " + std::to_string(userId) , this->db);
	dataHolder.clear();
}

/*
the function checks if the album exists in the data base
input: reference to the album name, the user id
output: answer if it exist or not
*/
bool DataBaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	bool ans = true;
	//get the album by its name from the database
	selectBy("ALBUMS","NAME = \"" + albumName + "\" AND USER_ID = " + std::to_string(userId), "*" ,this->db);
	//if there isn't any album with this name and owner id
	if (dataHolder.size() == 0)
	{
		//change the answer to false
		ans = false;
	}
	else
	{
		//if there is, clear the dataholder
		dataHolder.clear();
	}
	//return the ans
	return ans;
}
/*
the function open an album
input: the name of the album
output: the opened album
*/
Album DataBaseAccess::openAlbum(const std::string& albumName)
{
	bool Found = false;
	//get all the albums
	std::list<Album> albums = this->getAlbums();
	//go over the list until the album found
	for (auto& album : albums) {
		//when found
		if (albumName == album.getName())
		{
			//allocate memory to the pointer who hold the album
			this->opened_AL = new Album(album);
			//update it and the answer
			updateAl(this->opened_AL);
			Found = true;
		}
	}
	if (!Found)
	{
		throw MyException("No album with name " + albumName + " exists");
	}
	//clear the dataHolder and return the album
	dataHolder.clear();
	return *this->opened_AL;
}

/*
the function add picture to an album by it name
input: the album name and refernce to the picture object
output: NONE
*/
void DataBaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	dataHolder.clear();
	//get the album where to insert the picture
	selectBy("ALBUMS", "NAME = \"" + albumName + "\"", "ID", this->db);
	//insert it to the data base
	insertTo("PICTURES", "(ID, NAME, LOCATION, CREATION_DATE, ALBUM_ID)", "("+ std::to_string(picture.getId()) +  ", \"" + picture.getName() + "\", \"" + picture.getPath() + "\", \"" + picture.getCreationDate() + "\", " + dataHolder[0] + ")", this->db);
	//clear the data holder
	dataHolder.clear();
}

/*
the function remove picture from an album by it name
input: the album name and the picture name
output: NONE
*/
void DataBaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	//get the album to remove frome
	selectBy("ALBUMS", "NAME = " + albumName, "ID", this->db);
	//remove feom the picture
	deleteBy("PICTURES", "NAME = " + pictureName + "AND ALBUM_ID = "+ dataHolder[0] , this->db);
	//clear the data holder
	dataHolder.clear();
}

/*
the function print all the user of the system
*/
void DataBaseAccess::printUsers()
{
	//print topic and create sql request
	char* sqlite3_errmsg = nullptr;
	std::cout << "Users list:" << std::endl;
	std::cout << "-----------" << std::endl;
	std::string SQL = "SELECT * FROM USERS";
	//send it
	int res = sqlite3_exec(db, SQL.c_str(), callBack, nullptr, &sqlite3_errmsg);
	//goes over the users and print them
	for (unsigned int i  = 0; i < dataHolder.size(); i += 2)
	{
		std::cout << User(stoi(dataHolder[i]), dataHolder[i + 1]) << std::endl;
	}
	//clear the data holder
	dataHolder.clear();
}
/*
the function get user by its id
input: the user id
output: object of the user
*/
User DataBaseAccess::getUser(int userId)
{
	//get the user data
	selectBy("USERS", "ID = " + std::to_string(userId), "*", this->db);
	//create pointer to user object
	User * returnUser = new User(stoi(dataHolder[0]), dataHolder[1]);
	//clear the data holder and return the user 
	dataHolder.clear();
	return *returnUser;
}
/*
the function insert user to the database
input: reference to the user object to insert
output: NONE
*/
void DataBaseAccess::createUser(User& user)
{
	//insert it to the database
	insertTo("USERS", "(ID, NAME)", "(" + std::to_string(user.getId()) + ", " + "'"+ user.getName() + "')", this->db);
	//clear the dataholder
	dataHolder.clear();
}
/*
the function delete user from the data base
input: reference to the user object to insert
output: NONE
*/
void DataBaseAccess::deleteUser(const User& user)
{	
	//delete tags
	deleteBy("TAGS", "USER_ID = " + std::to_string(user.getId()), this->db);
	
	//delete all the pictures
	selectBy("ALBUMS", "USER_ID = " + std::to_string(user.getId()), "ID", this->db);
	for (unsigned int i = 0; i < dataHolder.size(); i += 4)
	{
		deleteBy("PICTURES", "ALBUM_ID = " + dataHolder[i], this->db);
	}
	
	//delete albums
	deleteBy("ALBUMS", "USER_ID = " + std::to_string(user.getId()), this->db);
	
	//delete user:
	deleteBy("USERS", "ID = " + std::to_string(user.getId()), this->db);
	dataHolder.clear();

}

/*
the function checks if the user exists in the data base
input: the user id
output: answer - yes or no
*/
bool DataBaseAccess::doesUserExists(int userId)
{
	bool ans = true;
	//get the user from the database
	selectBy("USERS", "ID = " + std::to_string(userId), "*", this->db);
	//if there isn't any data about it update ans to false
	if (dataHolder.size() == 0) { ans = false; }
	//clear the dataholder and return ans
	dataHolder.clear();
	return ans;
}

/*
the function add tag of an user to the data base
input: the album na,e, the picture name and the user id
output: NONE
*/
void DataBaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	//get the album of the picture to tag
	selectBy("ALBUMS", "NAME = \"" + albumName + "\"", "ID", this->db);
	//get the picture to tag
	selectBy("PICTURES", "ALBUM_ID = " + dataHolder[0] + " AND NAME = \"" + pictureName+ "\"", "ID", this->db);
	//insert tag to the tags table
	insertTo("TAGS", "(PICTURE_ID, USER_ID)", "(" + dataHolder[1] + ", " + std::to_string(userId) + ")", this->db);
	//clear the dataholder
	dataHolder.clear();
}
/*
the function delete tag of an user from the data base
input: the album name, the picture name and the user id
output: NONE
*/
void DataBaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	//get the album of the picture to remove the tag
	selectBy("ALBUMS", "NAME = \"" + albumName + "\"", "ID", this->db);
	//get the picture to remove the tag
	selectBy("PICTURES", "ALBUM_ID = " + dataHolder[0] + " AND NAME = \"" + pictureName + "\"", "ID", this->db);
	//delete tag from the tags table
	deleteBy("TAGS", "USER_ID = " + std::to_string(userId) + " AND PICTURE_ID = " + dataHolder[1], this->db);
	//clear the dataholder
	dataHolder.clear();
}
/*
the function count the amount of the albums of a user
input: reference to the user object
output: the amount of the albums
*/
int DataBaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	//get all the id of the albums of the user from the database
	selectBy("ALBUMS", "USER_ID = " + std::to_string(user.getId()), "USER_ID", this->db);
	//get the amount of them, clear the dataHolder and return the amount.
	int amountOfAlbums = dataHolder.size();
	dataHolder.clear();
	return amountOfAlbums;
}
/*
the function count the amount of albums that the user has a tag in them.
input: reference to the user object
output: the amount of the tags
*/
int DataBaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	int returnVal = 0;
	char* sqlite3_errmsg = nullptr;
	//build sql request (make inner join with 3 tables and use the count method of sqlite3)
	std::string SQL = "SELECT COUNT(DISTINCT ALBUMS.ID) FROM((ALBUMS INNER JOIN PICTURES ON PICTURES.ALBUM_ID = ALBUMS.ID) INNER JOIN TAGS ON PICTURES.ID = TAGS.PICTURE_ID) WHERE ALBUMS.USER_ID = " + std::to_string(user.getId());
	//send it
	int res = sqlite3_exec(db, SQL.c_str(), callBack, nullptr, &sqlite3_errmsg);
	//check if there is ant errors
	if (sqlite3_errmsg != nullptr)
	{
		std::cout << std::string(sqlite3_errmsg) << std::endl;
	}
	else
	{
		//get the amount of the tags, clear the database
		returnVal = stoi(dataHolder[0]);
		dataHolder.clear();
	}
	//return the amount
	return returnVal;
}
/*
the function count the amount of the tags that user have
input: reference to the user object
output: the amount of tags
*/
int DataBaseAccess::countTagsOfUser(const User& user)
{
	//get all the tags of the user from the database
	selectBy("TAGS", "USER_ID = " + std::to_string(user.getId()), "USER_ID", this->db);
	//get the amount, clean the dataholder and return the amount
	int amountOfTags = dataHolder.size();
	dataHolder.clear();
	return amountOfTags;
}
/*
the function calculate the avarage of tags per album that user have
input: reference to the user object
output: the answer
*/
float DataBaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	int albumsTaggedCount = countAlbumsTaggedOfUser(user);
	return 0 == albumsTaggedCount? 0 : (float)(countTagsOfUser(user)) / albumsTaggedCount ;
}

/*
the function get the top tagged user from the data base
input: NONE
output: object of the top tagged user
*/
User DataBaseAccess::getTopTaggedUser()
{
	//declear
	User* us = nullptr;
	char* sqlite3_errmsg = nullptr;
	//create sql request and send it (print if there is any errors)
	std::string SQL  = "SELECT USERS.ID, USERS.NAME FROM USERS JOIN TAGS ON TAGS.USER_ID = USERS.ID GROUP BY TAGS.PICTURE_ID ORDER BY COUNT(*) DESC LIMIT 1;";
	int res = sqlite3_exec(db, SQL.c_str(), callBack, nullptr, &sqlite3_errmsg);
	if (sqlite3_errmsg != nullptr) { std::cout << std::string(sqlite3_errmsg) << std::endl; }
	//if there is user
	if (dataHolder.size() != 0) 
	{
		//create new object of it and clear the dataholder
		us = new User(stoi(dataHolder[0]), dataHolder[1]);
		dataHolder.clear();
	}
	//builed an empty user
	else {us = new User(-1, "");}
	//return the user
	return *us;
}

/*
the function get the top tagged picture from the data base
input: NONE
output: object of the top tagged picture
*/
Picture DataBaseAccess::getTopTaggedPicture()
{
	//declear
	Picture * pic = nullptr; char* sqlite3_errmsg = nullptr;
	//create sql request and send it (print if there is any errors)
	std::string SQL = "SELECT PICTURES.ID, PICTURES.NAME, PICTURES.LOCATION,  PICTURES.CREATION_DATE FROM PICTURES JOIN TAGS ON TAGS.PICTURE_ID = PICTURES.ID GROUP BY TAGS.PICTURE_ID ORDER BY COUNT(*) DESC LIMIT 1;";
	int res = sqlite3_exec(db, SQL.c_str(), callBack, nullptr, &sqlite3_errmsg);
	if (sqlite3_errmsg != nullptr) {std::cout << std::string(sqlite3_errmsg) << std::endl;}
	//if there is any picture
	if (dataHolder.size() != 0)
	{
		//create new object of it and clear the dataholder
		pic = new Picture(stoi(dataHolder[0]), dataHolder[1], dataHolder[2], dataHolder[3]);
		dataHolder.clear();
	}
	//builded empty user
	else{pic = new Picture(-1, "");}
	//return the picture
	return *pic;
}
/*
the function get all the picture that specific user tagged in them
input: reference to the user object
output: list of the tagged picture
*/
std::list<Picture> DataBaseAccess::getTaggedPicturesOfUser(const User& user)
{
	//declear
	char* sqlite3_errmsg = nullptr;
	std::list<Picture> pictures;
	//create sql request and send it (print if there is any errors)
	std::string SQL = "SELECT PICTURE_ID, NAME, LOCATION, CREATION_DATE FROM TAGS INNER JOIN PICTURES ON PICTURES.ID = TAGS.PICTURE_ID WHERE TAGS.USER_ID = " + std::to_string(user.getId());
	int res = sqlite3_exec(db, SQL.c_str(),callBack , nullptr, &sqlite3_errmsg);
	if (sqlite3_errmsg != nullptr){std::cout << std::string(sqlite3_errmsg) << std::endl;}
	//goes over the pictures and push them to the list
	for (size_t i = 0; i < dataHolder.size(); i += 4)
	{
		pictures.push_back(Picture(stoi(dataHolder[i]), dataHolder[i + 1], dataHolder[i + 2], dataHolder[i + 3]));
	}
	//clear the dataholder
	dataHolder.clear();

	//go over the picture list and "tag" the user in each picture from the list
	for (auto& picture : pictures)
	{
		//get the tags of the currnt picture
		selectBy("TAGS", "PICTURE_ID = " + std::to_string(picture.getId()), "USER_ID", this->db);
		for (size_t i = 0; i < dataHolder.size(); i++)
		{
			//"tag" the user
			picture.tagUser(stoi(dataHolder[i]));
		}
		//reset the dataHolder
		dataHolder.clear();
	}
	//clear the data holder
	dataHolder.clear();
	//return the list of the pictures 
	return pictures;
}
/*
the function send a delete sql request based on the parameters that given
input: src - from which table to delete
	   bywhat - by what conditions to delete
	   db - the database to delete from
output: NONE
*/
void DataBaseAccess::deleteBy(std::string src, std::string byWhat, sqlite3* db)
{
	char* sqlite3_errmsg = nullptr;
	std::string SQL = "DELETE FROM " + src +" WHERE " + byWhat + ";";
	int res = sqlite3_exec(db, SQL.c_str(), nullptr, nullptr, &sqlite3_errmsg);
	if (sqlite3_errmsg != nullptr)
	{
		std::cout << std::string(sqlite3_errmsg) << std::endl;
	}
}
/*
the function send a select sql request based on the parameters that given
input : src - from which table to select
bywhat - by what conditions to select
what - what to get from the data base
db - the database to get from
output : NONE
*/
void DataBaseAccess::selectBy(std::string src, std::string byWhat, std::string what, sqlite3* db)
{
	char* sqlite3_errmsg = nullptr;
	std::string SQL = "SELECT " + what + " FROM " + src + " WHERE " + byWhat + ";";
	int res = sqlite3_exec(db, SQL.c_str(), callBack, nullptr, &sqlite3_errmsg);
	if (sqlite3_errmsg != nullptr)
	{
		std::cout << std::string(sqlite3_errmsg) << std::endl;
	}
	
}
/*
the function send an insert sql request based on the parameters that given
input : src - to which table to insert
bywhat - by what conditions to insert
what - what to insert from the data base
db - the database to insert to 
output : NONE
*/
void DataBaseAccess::insertTo(std::string toWhere, std::string headers, std::string what, sqlite3 * db)
{
	char* sqlite3_errmsg = nullptr;
	std::string SQL = "INSERT INTO " + toWhere + " " + headers + " VALUES " + what + ";";

	int res = sqlite3_exec(db, SQL.c_str(), nullptr, nullptr, &sqlite3_errmsg);
	if (sqlite3_errmsg != nullptr)
	{
		std::cout << std::string(sqlite3_errmsg) << std::endl;
	}
}

/*
the function update an album object with all the releted information of it in the database(tags, pictures)
input: pointer to the album object
output: NONE
*/
void DataBaseAccess::updateAl(Album* Al)
{
	//declear
	std::vector<std::string> picturesId; std::vector<Picture> pictures; std::string AlbumId;
	//get the album id and the pictures from it
	selectBy("ALBUMS", "NAME = \"" + Al->getName() + "\" AND USER_ID = " + std::to_string(Al->getOwnerId()), "ID", this->db);
	selectBy("PICTURES", "ALBUM_ID = " + dataHolder[0], "ID", this->db);
	
	//if there is any pictures
	if (dataHolder.size() > 1)
	{
		//save all the relevent data 
		AlbumId = dataHolder[0];
		for (size_t i = 1; i < dataHolder.size(); i++)
		{
			picturesId.push_back(dataHolder[i]);
		}
		dataHolder.clear();

		//get all the pictuers into a vector 
		selectBy("PICTURES", "ALBUM_ID = " + AlbumId, "*", this->db);
		for (size_t i = 0; i < dataHolder.size(); i += 5)
		{
			pictures.push_back(Picture(stoi(dataHolder[i]), dataHolder[i + 1], dataHolder[i + 2], dataHolder[i + 3]));
		}
		dataHolder.clear();
		
		//get all the tags of a specific picture into it, do it to all of the album pictures
		for (size_t i = 0; i < pictures.size(); i++)
		{
			selectBy("TAGS", "PICTURE_ID = " + std::to_string(pictures[i].getId()), "*", this->db);
			if (dataHolder.size() != 0)
			{
				for (size_t i = 0; i < pictures.size(); i++)
				{
					for (size_t k = 0; k < dataHolder.size(); k += 2)
					{
						pictures[i].tagUser(stoi(dataHolder[k + 1]));
					}
				}
				dataHolder.clear();
			}
		}

		//add all the updated pictures into the album
		for (size_t i = 0; i < pictures.size(); i++)
		{
			Al->addPicture(pictures[i]);
		}
	}
	
}

/*
the callback function that i used.
the function gets all the data from the data base and order it in a vector of strings, the vector is global
*/
int callBack(void* data, int argc, char** argv, char** azColName)
{
	int i = 0;
	for (i = 0; i < argc; i++)
	{
		dataHolder.push_back(argv[i]);
	}
	return 0;
}
